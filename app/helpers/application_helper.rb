module ApplicationHelper
    def flash_class(level)
        case level
            when "notice" then "alert-primary"
            when "success" then "alert-success"
            when "error" then "alert-danger"
            else "alert-warning"
        end
    end

    def active_class(link_path)
        current_page?(link_path) ? "active" : ""
    end

    def current_user_without_company?
        user_signed_in? && !current_user.company
    end

    def current_user_with_company?
        user_signed_in? && current_user.company
    end

    def current_user_is_company_admin?(company)
        user_signed_in? && current_user.admin && current_user.company_id == company.id
    end

end
