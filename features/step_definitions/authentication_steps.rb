Given("I am not signed in") do
    visit root_path
    if page.has_text?('Sign out')
        click_on 'Sign out'
    end
end

When("I fill the sign up form") do
    @user = FactoryBot.build(:user)
    visit sign_up_path
    fill_in "user_email", with: @user.email
    fill_in "user_password", with: @user.password
    fill_in "user_password_confirmation", with:@user.password
    click_on "Save"
end

Then("I should see that my account is created") do
    expect(page).to have_content("You have signed up successfully")
end

Given("I am a registered user") do
    @user = FactoryBot.create(:user)
end

When("I fill the sign in form") do
    visit sign_in_path
    fill_in "user_email", with: @user.email
    fill_in "user_password", with: @user.password
    click_on "Log in"
end

Then("I should be signed in") do
    expect(page).to have_content("Sign out")
    expect(page).not_to have_content("Sign in")
end

When("I sign out") do
    click_on "My account"
    click_on "Sign out"
end

Then("I should be signed out") do
    expect(page).not_to have_content("Sign out")
    expect(page).to have_content("Sign in")
end

When("I edit my profile") do
    click_on("My account")
    click_on("Profile")
    @new_user_details = FactoryBot.build(:user)
    fill_in "user_email", with: @new_user_details.email
    fill_in "user_name", with: @new_user_details.name
    fill_in "user_image", with: @new_user_details.image
    fill_in "user_current_password", with: @user.password
    click_on "Update"
end

Then("I should see the changes") do
    visit profile_path
    expect(find_field("user_email").value).to eq @new_user_details.email
    expect(find_field("user_name").value).to eq @new_user_details.name
    expect(find_field("user_image").value).to eq @new_user_details.image
end

When("I edit my password") do
    click_on("My account")
    click_on("Profile")
    @new_user_details = FactoryBot.build(:user)
    fill_in "user_password", with: @new_user_details.password
    fill_in "user_password_confirmation", with: @new_user_details.password
    fill_in "user_current_password", with: @user.password
    click_on "Update"
end

When("I sign in with the new password") do
    visit sign_in_path
    fill_in "user_email", with: @user.email
    fill_in "user_password", with: @new_user_details.password
    click_on "Log in"
end
