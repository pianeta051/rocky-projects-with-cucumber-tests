# Sign in / Sign up

Project to simulate a user registration and manage the profile.  Please download and test it :

```bash
git clone https://pianeta051@bitbucket.org/pianeta051/rocky-projects-with-cucumber-tests.git
cd rocky-projects-with-cucumber-tests
bundle
yarn install
rails db:migrate
rails s
```
